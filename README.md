# Sensors and Sensing Projects

- [Arduino UNO weather station](arduino_weather_station)
- [Ultrasonic Sensor via rosserial](https://create.arduino.cc/editor/ramilsafnab1996/ae9e4bd0-3d9b-41ed-9ecc-f5c878427569/preview)
- [Motor control](https://create.arduino.cc/editor/ramilsafnab1996/b469d064-d45a-496c-9830-04ca47af0124/preview)