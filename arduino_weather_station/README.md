# Arduino UNO weather station

<div align="center">
    <img src="arduino_weather_station/assets/Arduino weather station setup.jpg"></img>
    <div align="center">
        <b>Fig. 1 Arduino UNO and DHT 11 sensor setup</b>
    </div>
</div>

## Build and Run

```sh
catkin_make --pkg arduino_weather_station

roslaunch arduino_weather_station weather_station
```

This will start `rosserial` node which starts to publish topics for each message coming from the `ttyXXX` (connected to Arduino).

Arduino periodically reads data from the DHT 11 temperature and relative humidity sensor and writes messages to the serial port using ros wrapper ([Arduino Online Editor](https://create.arduino.cc/editor/ramilsafnab1996/17008c2c-99fc-4793-8c2c-e4f2bcbd6f27/preview)).

Data from the published topics (`/temperature` and `/relative_humidity`) is stored in the [ROS bag file](arduino_weather_station/bags/recordings_2019-05-17-20-26-28.bag).
